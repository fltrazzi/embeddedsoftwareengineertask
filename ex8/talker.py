#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import random

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
	random.seed(rospy.get_time())
	random_numbers = [random.randint(1,100) for i in range(10)]
	str_random_numbers=""
	for num in random_numbers:
		str_random_numbers += str(num) +" "
	rospy.loginfo(str_random_numbers)
        pub.publish(str_random_numbers)        
	rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass