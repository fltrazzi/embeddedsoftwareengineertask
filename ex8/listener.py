#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import numpy as np

def callback(data):
	str_numbers = data.data.split()
	numbers = [];
	for num in str_numbers:
		numbers.append(int(num))
	mean = np.mean(numbers)
	std = np.std(numbers)
	rospy.loginfo('Mean = %.2f ; Std = %.2f', mean, std)

def listener():
	rospy.init_node('listener', anonymous=True)
	rospy.Subscriber('chatter', String, callback)
	rospy.spin()

if __name__ == '__main__':
	listener()
