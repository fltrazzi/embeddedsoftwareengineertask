#include <iostream>
using namespace std;

class CheckPayment{
protected:
	double amountDue=0.0;
private: 
	string client;
public:
	CheckPayment(double a) {amountDue = a;}
	~CheckPayment(){}
	void addAmoutDue(double a) {amountDue += a;}
	void setClient(string c) {client = c;}
	double getAmountDue() {return amountDue;}
	string getClient() {return client;}
	virtual pay() {amountDue=0;}
};

class Cash : public CheckPayment{
private: 
	double change=0.0;
public:
	Cash(double a) : CheckPayment(a) {}
	~Cash(){}
	pay() {cout << "Payment: $" << amountDue << " (cash)" << endl; amountDue=0;}
	pay(double cash){cout << "Payment: $" << cash << " (cash)" << endl; change=cash-amountDue; amountDue=0;}
	double getChange() {return change;}
};

class CreditCard : public CheckPayment{
private: 
	string brand;
public:
	CreditCard(double a, string b) : CheckPayment(a) {brand=b;}
	~CreditCard(){}
	setBrand(string b) {brand=b;}
	string getBrand() {return brand;}
	pay() {cout << "Payment: $" << amountDue << " (credit card)" << endl; amountDue=0;}
};


int main(int argc, char** argv) {
	Cash *csh = new Cash(100);
	csh->setClient("Fernando");
	csh->addAmoutDue(50);
	cout << "Client: " << csh->getClient() << endl;
	cout << "Amount due: $" << csh->getAmountDue() << endl;
	csh->pay(200);
	cout << "Change: $" << csh->getChange() << endl;
	cout << "Amount due: $" << csh->getAmountDue() << endl << endl;
	
	CreditCard *cc = new CreditCard(300,"Visa");
	cout << "Brand: " << cc->getBrand() << endl;
	cout << "Amount due: $" << cc->getAmountDue() << endl;
	cc->pay();
	cout << "Amount due: $" << cc->getAmountDue() << endl;
	
	delete csh;
	delete cc;
	
	return 0;
}