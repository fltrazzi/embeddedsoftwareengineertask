#include <iostream>
#include <windows.h>
#include <process.h>
#include <time.h>

using namespace std;

int num[10];
CRITICAL_SECTION criticalSection;

void updateRandNum( void* pParams ) {
    srand (time(NULL));
    while (1) {
        cout << "Update random numbers" << endl;
        EnterCriticalSection(&criticalSection);
        for (int i=0 ; i < 10 ; i++) {
        	num[i] = rand() % 100;
        	cout << num [i] << ", ";
		}
		cout << endl << "----------------" << endl;
        LeaveCriticalSection(&criticalSection);
        Sleep(500);
    }
}

void readRandNum() {
    cout << "Read current random numbers" << endl;
    EnterCriticalSection(&criticalSection);
    for (int i=0 ; i < 10 ; i++) {
    	cout << num [i] << ", ";
	}
	cout << endl << "----------------" << endl;
    LeaveCriticalSection(&criticalSection);
    Sleep(2000);
}

int main() {
    InitializeCriticalSection(&criticalSection);
    _beginthread(updateRandNum, 0, NULL);
    for (int i = 0 ; i < 5; i++) {
    	readRandNum();
    }
    DeleteCriticalSection(&criticalSection);
    return 0;
}